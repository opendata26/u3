package com.lbe.bootstrap;

import android.os.Binder;
import android.util.SparseIntArray;

import com.lbe.security.utility.RSAVerifier;

public class AuthService {
	
	private SparseIntArray uidMap;
	
	public AuthService() {
		uidMap = new SparseIntArray();
	}
	
	public boolean registerCaller(String apiKey) {
		int uid = Binder.getCallingUid(), pid = Binder.getCallingPid();
		int result = uidMap.get(uid);
		if (result == 0) {
			result = RSAVerifier.validateRSAKey(apiKey, pid) ? 1 : -1;
			uidMap.put(uid, result);
		}
		return result == 1;
	}

	public boolean validateCaller() {
		int uid = Binder.getCallingUid();
		return uidMap.get(uid) == 1;
	}
}
