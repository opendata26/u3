#!/system/bin/sh

touch /data/aurora/currentfilelist
touch /data/aurora/md5_current.file
chmod 777 /data/aurora/currentfilelist
chmod 777 /data/aurora/md5_current.file

cat /dev/null > /data/aurora/currentfilelist
cat /dev/null > /data/aurora/md5_current.file
sync
isDir(){
    local dirName=$1
    if [ ! -d $dirName ]; then
    return 1
    else
    return 0
    fi
}

recursionDir(){
    local dir=$1
    local filelist=`ls  "${dir}"`
    for filename in $filelist
    do
        local fullpath="$dir"/"${filename}";
        if isDir "${fullpath}";then
            recursionDir "${fullpath}"
        else
            echo "${fullpath}"  >> /data/aurora/currentfilelist
        fi
    done
}

echo "recurse current file."
recursionDir /system/app
recursionDir /system/priv-app
recursionDir /system/bin
recursionDir /system/xbin
recursionDir /system/framework

echo "create current md5."
for file in `cat /data/aurora/currentfilelist`
do
#   [ -z "${file}" -o "${file:0:1}" == "#" ] && continue
   [ -z "${file}" -o "${file:0:1}" == "#" -o ${file:(-6):6} == "run-as" ] && continue
   tmp=`md5 ${file}`
   md5value=${tmp:0:32}
   echo "${file}:${md5value}" >> /data/aurora/md5_current.file
done
rm /data/aurora/currentfilelist
sync
