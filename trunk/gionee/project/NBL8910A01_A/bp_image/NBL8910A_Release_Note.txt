==================
Build Notes
==================
1.版本目的:
Update baseline to r3030472.1
2.修改的重大问题点:
Update baseline to r3030472.1
3.测试重点:
modem
4.应用数据库是否改变:
no

==================
Build Info
==================
Build Machine: BIANYI1-DESKTOP
Target: -opt=TARGET_BUILD_VARIANT=user

==================
Release Location
==================
FTP://18.8.8.2/software_release/NBL8910A/NBL8910A01_A_T1602.zip

==================
Software Version Description
==================


==================
CR List
==================
[Repository URL]: http://192.168.110.97/svn/modem_qc_kk_qct8974/trunk
[Tag        URL]: http://192.168.110.97/svn/modem_qc_kk_qct8974/tags/TAG_NBL8910A_T1602
[Current    Tag]: TAG_NBL8910A_T1602 (347)
[Previous   Tag]: TAG_NBL8910A_T1601 (352)

[Repository URL]: http://192.168.110.97/svn/packages_qc_modem/trunk
[Tag        URL]: http://192.168.110.97/svn/packages_qc_modem/tags/TAG_NBL8910A_T1602
[Current    Tag]: TAG_NBL8910A_T1602 (2870)
[Previous   Tag]: TAG_NBL8910A_T1601 (1)

