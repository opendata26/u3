/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
      
#define LOG_TAG "echonode"

#include <cutils/properties.h>
 
#include <binder/IPCThreadState.h>
#include <binder/ProcessState.h>
#include <binder/IServiceManager.h>

#include <utils/Log.h>
#include <utils/threads.h>

#if defined(HAVE_PTHREADS)
# include <pthread.h>
# include <sys/resource.h>
#endif

#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <linux/input.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/poll.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/un.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>

#include <ctype.h>
#include <limits.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>



using namespace android;
#if 1
#include <cutils/klog.h>
void klog_write(int level, const char *fmt, ...);
static int klog_fd = -1;
static int klog_level = KLOG_DEFAULT_LEVEL;
void klog_set_level(int level) {
        klog_level = level;
}
void klog_init(void)
{
        static const char *name = "/dev/__kmsg__";
            if (klog_fd >= 0) return; /* Already initialized */
                if (mknod(name, S_IFCHR | 0600, (1 << 8) | 11) == 0) {
                            klog_fd = open(name, O_WRONLY);
                                    fcntl(klog_fd, F_SETFD, FD_CLOEXEC);
                                            unlink(name);
                                                }
}
#define LOG_BUF_MAX 512
void klog_write(int level, const char *fmt, ...)
{
        char buf[LOG_BUF_MAX];
            va_list ap;
           // if (level > klog_level) return;
                    if (klog_fd < 0) klog_init();
                        va_start(ap, fmt);
                            vsnprintf(buf, LOG_BUF_MAX, fmt, ap);
                                buf[LOG_BUF_MAX - 1] = 0;
                                    va_end(ap);
                                        write(klog_fd, buf, strlen(buf));
}
#define LOGE(x...) do { KLOG_ERROR("echonode", x); } while (0)
#define LOGI(x...) do { KLOG_INFO("echonode", x); } while (0)
#define LOGV(x...) do { KLOG_DEBUG("echonode", x); } while (0)

#endif

static int write_file(const char *path, const char *value)
{
    int fd, ret, len;

    fd = open(path, O_WRONLY|O_CREAT, 0622);

    if (fd < 0)
        return -errno;

    len = strlen(value);

    do {
        ret = write(fd, value, len);
    } while (ret < 0 && errno == EINTR);

    close(fd);
    if (ret < 0) {
        return -errno;
    } else {
        return 0;
    }
}

char configpath[256] = "";
void parse_cfg(void)
{
    FILE *fp;
    char buf[256];
    char *pstrvalue;
    char *pstrpath;
    char *pstr;
    int len,i;
    char *cfgPath[]={
        "/storage/sdcard0/echonode.cfg",
        "/data/local/tmp/echonode.cfg",
        "/system/etc/echonode.cfg",
    };
    for(i=0;i<3;i++)
    {
        if((fp = fopen(cfgPath[i],"r")) != NULL)
        {
            strcpy(configpath,cfgPath[i]);
            printf("open %s  success!\n",configpath);
            LOGI("open %s  success!\n",configpath);
            break;
	}

    }
    if(fp == NULL)
        {
        strcpy(configpath,"sdcard/,data/local/tmp/,system/etc/,echonode.cfg");
        printf("open %s  error!\n",configpath);
            LOGI("open %s  error!\n",configpath);
        return;
    }

    while (!feof(fp))
    {
        fgets(buf,256,fp);
        len = strlen(buf);
	    LOGI("parse a line:%s,[len:%d]\n",buf,len);
        pstrvalue = strstr(buf,"echo");
        pstrpath = strstr(buf,">");
        if(pstrvalue && pstrpath)
        {
            LOGI("found echo cmd!\n");
            pstrvalue+=4;//skip echo
            if( (pstr = strstr(pstrvalue,"\"")) != 0)
            {
                //skip "
                pstrvalue = pstr+1;
            
                if( (pstr = strstr(pstrvalue,"\"")) != 0)
                {
                    //skip "
                    if(pstr < buf+len)
                    {
                        *pstr = '\0';
                    }
                }
            }
            else
            {
                //skip " "
                while(pstrvalue < pstrpath)
                {
                    if(*pstrvalue == ' ')
                    {
                        pstrvalue++;
                    }
                    else
                    {
                        break;
                    }
                }
                pstr = pstrpath-1;
                while(pstr > pstrvalue)
                {
                    if(*pstr == ' ')
                    {
                        *pstr = '\0';
                        pstr--;
                    }
                    else
                    {
                        break;
                    }
                }
            }

            
       

            if(pstrpath)
            {
                LOGI("found > \n");
                *pstrpath = '\0';
                pstrpath++;
                //skip " "
                while(*pstrpath != '\0' )
                {
                    if(*pstrpath == ' ')
                    {
                        pstrpath++;
                    }
                    else
                    {
                        break;
                    }
                }
                if( (pstr = strstr(pstrpath,"\r\n")) !=0 )
                {
                    //skip enterline
                    if(pstr < buf+len)
                    {
                        *pstr = '\0';
                    }
                }

                if(pstrvalue < (buf+len) && pstrpath < (buf+len))
                {
                    LOGI("vaule:%s!\n",pstrvalue);
                    LOGI("path:%s!\n",pstrpath);
                    write_file(pstrpath,pstrvalue);
                }
            }
            
        }
        else
        {
           
            pstrvalue = strstr(buf,"[ES]");
            if(pstrvalue)
            {
                pstrvalue+=4;//skip [ES]
                if( (pstr = strstr(pstrvalue,"\r\n")) !=0 )
                {
                    //skip enterline
                    if(pstr < buf+len)
                    {
                        *pstr = '\0';
                    }

                }
                LOGI("[ES]:%s!\n",pstrvalue);
                if(*pstrvalue != '\0')
                {
                    system(pstrvalue);
                }
            }

        }
    }
    fclose(fp);
}



int main(int argc, char** argv)
{
        //char* argv[] = {"/system/bin/cat","/proc/kmsg"," > /data/local/1", (char*)0};
        //execv( "/system/bin/cat", argv);
        //execl( "/system/bin/cat", "cat", "/system/build.prop", " > /data/local/tmp/1", NULL);
	int pid = fork();
	int result = 0;
	
      //klog_init();
	  //klog_set_level(6);
      //LOGI("--------------- enter save poweroff chg log ---------------\n"); 

      if(pid <0) 
      {
          printf("fork error");  
          //LOGI("fork error");  
      }
      
      if (pid == 0)
      {
      	printf("I am child process, My process id is %d\n", getpid()); 
	 klog_init();
	    LOGI("daemon running\n");
      	//LOGI("child process running ...\n"); 
#if 0
	//	system("/system/bin/mount -t vfat -r -w /dev/block/mmcblk0p20 /storage/sdcard0/");
		system("/system/bin/mkdir /storage/sdcard0/test");
		if (access("/storage/sdcard0/test/", F_OK) != 0)
		{
			printf("sdcard mount error\n");  
			LOGI("sdcard mount error\n");  
			parse_cfg(0);
		}
		else
		{
			printf("sdcard mount success\n"); 
			LOGI("sdcard mount success\n"); 
			system("/system/bin/rm /storage/sdcard0/test");
			parse_cfg(1);
		}
#else
			parse_cfg();
#endif

		
		printf("%s\n", configpath);  
	    LOGI("daemon exit and configpath is:%s\n",configpath);
        printf("child process running over\n");
      }
      else
      {
      	printf("I am parent process, My process id is %d\n", getpid());
		//LOGI("parent process running\n"); 
      	waitpid(pid,&result,0);
        printf("parent process running over\n");
      }

      //LOGI("--------------- exit save poweroff chg log ---------------\n"); 
    return 0;
}
