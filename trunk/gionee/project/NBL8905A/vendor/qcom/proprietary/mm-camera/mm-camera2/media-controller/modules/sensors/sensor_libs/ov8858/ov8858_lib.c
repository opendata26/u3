/*============================================================================

  Copyright (c) 2013-2014 Qualcomm Technologies, Inc. All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.

============================================================================*/
#include <stdio.h>
#include "sensor_lib.h"

#define SENSOR_MODEL_NO_OV8858 "ov8858"
#define OV8858_LOAD_CHROMATIX(n) \
  "libchromatix_"SENSOR_MODEL_NO_OV8858"_"#n".so"

static sensor_lib_t sensor_lib_ptr;
#define OV8858_VIDEO_UPSCALE_1080P
static struct msm_sensor_power_setting power_setting[] = {
  {
    .seq_type = SENSOR_VREG,
    .seq_val = CAM_VIO,
    .config_val = 0,
    .delay = 1,
  },
  {
    .seq_type = SENSOR_VREG,
    .seq_val = CAM_VANA,
    .config_val = 0,
    .delay = 1,
  },
  {
	.seq_type = SENSOR_GPIO,
	.seq_val = SENSOR_GPIO_VDIG,   //SUB DVDD GPIO POWER
	.config_val = GPIO_OUT_HIGH,
	.delay = 5,
  },
  {
    .seq_type = SENSOR_VREG,
    .seq_val = CAM_VDIG,
    .config_val = 0,
    .delay = 1,
  },
  {
    .seq_type = SENSOR_GPIO,
    .seq_val = SENSOR_GPIO_STANDBY,
    .config_val = GPIO_OUT_LOW,
    .delay = 1,
  },
  {
    .seq_type = SENSOR_GPIO,
    .seq_val = SENSOR_GPIO_RESET,
    .config_val = GPIO_OUT_LOW,
    .delay = 5,
  },
  {
    .seq_type = SENSOR_GPIO,
    .seq_val = SENSOR_GPIO_STANDBY,
    .config_val = GPIO_OUT_HIGH,
    .delay = 5,
  },
  {
    .seq_type = SENSOR_GPIO,
    .seq_val = SENSOR_GPIO_RESET,
    .config_val = GPIO_OUT_HIGH,
    .delay = 10,
  },
  {
    .seq_type = SENSOR_CLK,
    .seq_val = SENSOR_CAM_MCLK,
    .config_val = 24000000,
    .delay = 10,
  },
  {
    .seq_type = SENSOR_I2C_MUX,
    .seq_val = 0,
    .config_val = 0,
    .delay = 0,
  },
};


static struct msm_camera_sensor_slave_info sensor_slave_info = {
  /* Camera slot where this camera is mounted */
  .camera_id = CAMERA_1,
  /* sensor slave address */
  .slave_addr = 0x6c,
  /* sensor address type */
  .addr_type = MSM_CAMERA_I2C_WORD_ADDR,
  /* sensor id info*/
  .sensor_id_info = {
    /* sensor id register address */
    .sensor_id_reg_addr = 0x300b,
    /* sensor id */
    .sensor_id = 0x8858,
  },
  /* power up / down setting */
  .power_setting_array = {
    .power_setting = power_setting,
    .size = ARRAY_SIZE(power_setting),
  },
};

static struct msm_sensor_init_params sensor_init_params = {
  .modes_supported = CAMERA_MODE_2D_B,
  .position = FRONT_CAMERA_B,
  .sensor_mount_angle = SENSOR_MOUNTANGLE_360,
};

static sensor_output_t sensor_output = {
  .output_format = SENSOR_BAYER,
  .connection_mode = SENSOR_MIPI_CSI,
  .raw_output = SENSOR_10_BIT_DIRECT,
};

static struct msm_sensor_output_reg_addr_t output_reg_addr = {
  .x_output = 0x3808,
  .y_output = 0x380a,
  .line_length_pclk = 0x380c,
  .frame_length_lines = 0x380e,
};

static struct msm_sensor_exp_gain_info_t exp_gain_info = {
  .coarse_int_time_addr = 0x3500,
  .global_gain_addr = 0x3508,
  .vert_offset = 8,
};

static sensor_aec_data_t aec_info = {
  .max_gain = 16.0,
  .max_linecount = 38808,
};

static sensor_lens_info_t default_lens_info = {
  .focal_length = 2.54,
  .pix_size = 1.12,
  .f_number = 2.4,
  .total_f_dist = 1.2,
  .hor_view_angle = 67.2,
  .ver_view_angle = 50.4,
};

static struct csi_lane_params_t csi_lane_params = {
  .csi_lane_assign = 0x4320,
  .csi_lane_mask = 0x1F,
  .csi_if = 1,
  .csid_core = {0},
  .csi_phy_sel = 2,
};

static struct msm_camera_i2c_reg_array init_reg_array0[] = {
  {0x0103, 0x01},
};

static struct msm_camera_i2c_reg_array init_reg_array1[] = {
	//@@5.1.1.1 Initialization (Global Setting)
//;; MIPI=720Mbps, SysClk=74.4Mhz,Dac Clock=360Mhz.
//;; v00_01_00 (05/29/2013) : initial setting
  {0x0100, 0x00},
  {0x0302, 0x1e},
  {0x0303, 0x00},
  {0x0304, 0x03},
  {0x030d, 0x1f},
  {0x030e, 0x02},
  {0x030f, 0x04},
  {0x0312, 0x03},
  {0x031e, 0x0c},
  {0x3007, 0x80},
  {0x3600, 0x00},
  {0x3601, 0x00},
  {0x3602, 0x00},
  {0x3603, 0x00},
  {0x3604, 0x22},
  {0x3605, 0x20},
  {0x3606, 0x00},
  {0x3607, 0x20},
  {0x3608, 0x11},
  {0x3609, 0x28},
  {0x360a, 0x00},
  {0x360b, 0x05},
  {0x360c, 0xd4},
  {0x360d, 0x40},
  {0x360e, 0x0c},
  {0x360f, 0x20},
  {0x3610, 0x07},
  {0x3611, 0x20},
  {0x3612, 0x88},
  {0x3613, 0x80},
  {0x3614, 0x58},
  {0x3615, 0x00},
  {0x3616, 0x4a},
  {0x3617, 0x40},
  {0x3618, 0x5a},
  {0x3619, 0x70},
  {0x361a, 0x99},
  {0x361b, 0x0a},
  {0x361c, 0x07},
  {0x361d, 0x00},
  {0x361e, 0x00},
  {0x361f, 0x00},
  {0x3638, 0xff},
  {0x3633, 0x0f},
  {0x3634, 0x0f},
  {0x3635, 0x0f},
  {0x3636, 0x12},
  {0x3645, 0x13},
  {0x3646, 0x83},
  {0x364a, 0x07},
  {0x3015, 0x00},
  {0x3018, 0x72},
  {0x3020, 0x93},
  {0x3022, 0x01},
  {0x3031, 0x0a},
  {0x3034, 0x00},
  {0x3106, 0x01},
  {0x3305, 0xf1},
  {0x3308, 0x00},
  {0x3309, 0x28},
  {0x330a, 0x00},
  {0x330b, 0x20},
  {0x330c, 0x00},
  {0x330d, 0x00},
  {0x330e, 0x00},
  {0x330f, 0x40},
  {0x3307, 0x04},
  {0x3500, 0x00},
  {0x3501, 0x4d},
  {0x3502, 0x40},
  {0x3503, 0x80},
  {0x3505, 0x80},
  {0x3508, 0x02},
  {0x3509, 0x00},
  {0x350c, 0x00},
  {0x350d, 0x80},
  {0x3510, 0x00},
  {0x3511, 0x02},
  {0x3512, 0x00},
  {0x3700, 0x18},
  {0x3701, 0x0c},
  {0x3702, 0x28},
  {0x3703, 0x19},
  {0x3704, 0x14},
  {0x3705, 0x00},
  {0x3706, 0x82},
  {0x3707, 0x04},
  {0x3708, 0x24},
  {0x3709, 0x33},
  {0x370a, 0x01},
  {0x370b, 0x82},
  {0x370c, 0x04},
  {0x3718, 0x12},
  {0x3719, 0x31},
  {0x3712, 0x42},
  {0x3714, 0x24},
  {0x371e, 0x19},
  {0x371f, 0x40},
  {0x3720, 0x05},
  {0x3721, 0x05},
  {0x3724, 0x06},
  {0x3725, 0x01},
  {0x3726, 0x06},
  {0x3728, 0x05},
  {0x3729, 0x02},
  {0x372a, 0x03},
  {0x372b, 0x53},
  {0x372c, 0xa3},
  {0x372d, 0x53},
  {0x372e, 0x06},
  {0x372f, 0x10},
  {0x3730, 0x01},
  {0x3731, 0x06},
  {0x3732, 0x14},
  {0x3733, 0x10},
  {0x3734, 0x40},
  {0x3736, 0x20},
  {0x373a, 0x05},
  {0x373b, 0x06},
  {0x373c, 0x0a},
  {0x373e, 0x03},
  {0x3750, 0x0a},
  {0x3751, 0x0e},
  {0x3755, 0x10},
  {0x3758, 0x00},
  {0x3759, 0x4c},
  {0x375a, 0x06},
  {0x375b, 0x13},
  {0x375c, 0x20},
  {0x375d, 0x02},
  {0x375e, 0x00},
  {0x375f, 0x14},
  {0x3768, 0xcc},
  {0x3769, 0x44},
  {0x376a, 0x44},
  {0x3761, 0x00},
  {0x3762, 0x00},
  {0x3763, 0x18},
  {0x3766, 0xff},
  {0x376b, 0x00},
  {0x3772, 0x23},
  {0x3773, 0x02},
  {0x3774, 0x16},
  {0x3775, 0x12},
  {0x3776, 0x04},
  {0x3777, 0x00},
  {0x3778, 0x17},
  {0x37a0, 0x44},
  {0x37a1, 0x3d},
  {0x37a2, 0x3d},
  {0x37a3, 0x00},
  {0x37a4, 0x00},
  {0x37a5, 0x00},
  {0x37a6, 0x00},
  {0x37a7, 0x44},
  {0x37a8, 0x4c},
  {0x37a9, 0x4c},
  {0x3760, 0x00},
  {0x376f, 0x01},
  {0x37aa, 0x44},
  {0x37ab, 0x2e},
  {0x37ac, 0x2e},
  {0x37ad, 0x33},
  {0x37ae, 0x0d},
  {0x37af, 0x0d},
  {0x37b0, 0x00},
  {0x37b1, 0x00},
  {0x37b2, 0x00},
  {0x37b3, 0x42},
  {0x37b4, 0x42},
  {0x37b5, 0x31},
  {0x37b6, 0x00},
  {0x37b7, 0x00},
  {0x37b8, 0x00},
  {0x37b9, 0xff},
  {0x3800, 0x00},
  {0x3801, 0x0c},
  {0x3802, 0x00},
  {0x3803, 0x0c},
  {0x3804, 0x0c},
  {0x3805, 0xd3},
  {0x3806, 0x09},
  {0x3807, 0xa3},
  {0x3808, 0x06},
  {0x3809, 0x60},
  {0x380a, 0x04},
  {0x380b, 0xc8},
  {0x380c, 0x07},
  {0x380d, 0x88},
  {0x380e, 0x04},
  {0x380f, 0xdc},
  {0x3810, 0x00},
  {0x3811, 0x04},
  {0x3813, 0x02},
  {0x3814, 0x03},
  {0x3815, 0x01},
  {0x3820, 0x00},
  {0x3821, 0x67},
  {0x382a, 0x03},
  {0x382b, 0x01},
  {0x3830, 0x08},
  {0x3836, 0x02},
  {0x3837, 0x18},
  {0x3841, 0xff},
  {0x3846, 0x48},
  {0x3d85, 0x16},
  {0x3d8c, 0x73},
  {0x3d8d, 0xde},
  {0x3f08, 0x08},
  {0x3f0a, 0x00},
  {0x4000, 0xf1},
  {0x4001, 0x10},
  {0x4005, 0x10},
  {0x4002, 0x27},
  {0x4009, 0x83},
  {0x400a, 0x01},
  {0x400b, 0x0c},
  {0x400d, 0x10},
  {0x4011, 0x20},
  {0x401b, 0x00},
  {0x401d, 0x00},
  {0x4020, 0x00},
  {0x4021, 0x04},
  {0x4022, 0x04},
  {0x4023, 0xb9},
  {0x4024, 0x05},
  {0x4025, 0x2a},
  {0x4026, 0x05},
  {0x4027, 0x2b},
  {0x4028, 0x00},
  {0x4029, 0x02},
  {0x402a, 0x04},
  {0x402b, 0x04},
  {0x402c, 0x02},
  {0x402d, 0x02},
  {0x402e, 0x08},
  {0x402f, 0x02},
  {0x401f, 0x00},
  {0x4034, 0x3f},
  {0x403d, 0x04},
  {0x403e, 0x08},
  {0x4040, 0x07},
  {0x4041, 0xc6},
  {0x4300, 0xff},
  {0x4202, 0x00},
  {0x4301, 0x00},
  {0x4302, 0x0f},
  {0x4316, 0x00},
  {0x4500, 0x58},
  {0x4503, 0x18},
  {0x4600, 0x00},
  {0x4601, 0xcb},
  {0x470b, 0x28},
  {0x481f, 0x32},
  {0x4837, 0x16},
  {0x4850, 0x10},
  {0x4851, 0x32},
  {0x4b00, 0x2a},
  {0x4b0d, 0x00},
  {0x4d00, 0x04},
  {0x4d01, 0x18},
  {0x4d02, 0xc3},
  {0x4d03, 0xff},
  {0x4d04, 0xff},
  {0x4d05, 0xff},
  #if 0 /* enable lenc, tanrifei, 20141225*/
  {0x5000, 0x7e},
  #else
  {0x5000, 0xfe},
  #endif /* end */
  {0x5001, 0x01},
  {0x5002, 0x08},
  {0x5003, 0x20},
  {0x5046, 0x12},
  {0x5780, 0x3e},
  {0x5781, 0x0f},
  {0x5782, 0x44},
  {0x5783, 0x02},
  {0x5784, 0x01},
  {0x5785, 0x00},
  {0x5786, 0x00},
  {0x5787, 0x04},
  {0x5788, 0x02},
  {0x5789, 0x0f},
  {0x578a, 0xfd},
  {0x578b, 0xf5},
  {0x578c, 0xf5},
  {0x578d, 0x03},
  {0x578e, 0x08},
  {0x578f, 0x0c},
  {0x5790, 0x08},
  {0x5791, 0x04},
  {0x5792, 0x00},
  {0x5793, 0x52},
  {0x5794, 0xa3},
  {0x5871, 0x0d},
  {0x5870, 0x18},
  {0x586e, 0x10},
  {0x586f, 0x08},
  {0x58f8, 0x3d},
  {0x5901, 0x00},
  {0x5b00, 0x02},
  {0x5b01, 0x10},
  {0x5b02, 0x03},
  {0x5b03, 0xcf},
  {0x5b05, 0x6c},
  {0x5e00, 0x00},               
  {0x5e01, 0x41},               
  {0x382d, 0x7f},               
  {0x4825, 0x3a},               
  {0x4826, 0x40},               
  {0x4808, 0x25},               
//  {0x0100, 0x01},               
};

static struct msm_camera_i2c_reg_setting init_reg_setting[] = {
  {
    .reg_setting = init_reg_array0,
    .size = ARRAY_SIZE(init_reg_array0),
    .addr_type = MSM_CAMERA_I2C_WORD_ADDR,
    .data_type = MSM_CAMERA_I2C_BYTE_DATA,
    .delay = 50,
  },
  {
    .reg_setting = init_reg_array1,
    .size = ARRAY_SIZE(init_reg_array1),
    .addr_type = MSM_CAMERA_I2C_WORD_ADDR,
    .data_type = MSM_CAMERA_I2C_BYTE_DATA,
    .delay = 0,
  },
};

static struct sensor_lib_reg_settings_array init_settings_array = {
  .reg_settings = init_reg_setting,
  .size = 2,
};

static struct msm_camera_i2c_reg_array start_reg_array[] = {
  {0x0100, 0x01},
};

static  struct msm_camera_i2c_reg_setting start_settings = {
  .reg_setting = start_reg_array,
  .size = ARRAY_SIZE(start_reg_array),
  .addr_type = MSM_CAMERA_I2C_WORD_ADDR,
  .data_type = MSM_CAMERA_I2C_BYTE_DATA,
  .delay = 0,
};

static struct msm_camera_i2c_reg_array stop_reg_array[] = {
  {0x0100, 0x00},
};

static struct msm_camera_i2c_reg_setting stop_settings = {
  .reg_setting = stop_reg_array,
  .size = ARRAY_SIZE(stop_reg_array),
  .addr_type = MSM_CAMERA_I2C_WORD_ADDR,
  .data_type = MSM_CAMERA_I2C_BYTE_DATA,
  .delay = 0,
};

static struct msm_camera_i2c_reg_array groupon_reg_array[] = {
  {0x3208, 0x00},
};

static struct msm_camera_i2c_reg_setting groupon_settings = {
  .reg_setting = groupon_reg_array,
  .size = ARRAY_SIZE(groupon_reg_array),
  .addr_type = MSM_CAMERA_I2C_WORD_ADDR,
  .data_type = MSM_CAMERA_I2C_BYTE_DATA,
  .delay = 0,
};

static struct msm_camera_i2c_reg_array groupoff_reg_array[] = {
  {0x3208, 0x10},
  {0x3208, 0xA0},
};

static struct msm_camera_i2c_reg_setting groupoff_settings = {
  .reg_setting = groupoff_reg_array,
  .size = ARRAY_SIZE(groupoff_reg_array),
  .addr_type = MSM_CAMERA_I2C_WORD_ADDR,
  .data_type = MSM_CAMERA_I2C_BYTE_DATA,
  .delay = 0,
};

static struct msm_camera_csid_vc_cfg ov8858_cid_cfg[] = {
  {0, CSI_RAW10, CSI_DECODE_10BIT},
  {1, CSI_EMBED_DATA, CSI_DECODE_8BIT},
};

static struct msm_camera_csi2_params ov8858_csi_params = {
  .csid_params = {
    .lane_cnt = 4,
    .lut_params = {
      .num_cid = 2,
      .vc_cfg = {
         &ov8858_cid_cfg[0],
         &ov8858_cid_cfg[1],
      },
    },
  },
  .csiphy_params = {
    .lane_cnt = 4,
    .settle_cnt = 0x14,
  },
};

static struct msm_camera_csi2_params *csi_params[] = {
  &ov8858_csi_params, /* RES 0*/
  &ov8858_csi_params, /* RES 1*/
};

static struct sensor_lib_csi_params_array csi_params_array = {
  .csi2_params = &csi_params[0],
  .size = ARRAY_SIZE(csi_params),
};

static struct sensor_pix_fmt_info_t ov8858_pix_fmt0_fourcc[] = {
  { V4L2_PIX_FMT_SBGGR10 },
};

static struct sensor_pix_fmt_info_t ov8858_pix_fmt1_fourcc[] = {
  { MSM_V4L2_PIX_FMT_META },
};

static sensor_stream_info_t ov8858_stream_info[] = {
  {1, &ov8858_cid_cfg[0], ov8858_pix_fmt0_fourcc},
  {1, &ov8858_cid_cfg[1], ov8858_pix_fmt1_fourcc},
};

static sensor_stream_info_array_t ov8858_stream_info_array = {
  .sensor_stream_info = ov8858_stream_info,
  .size = ARRAY_SIZE(ov8858_stream_info),
};

static struct msm_camera_i2c_reg_array res0_reg_array[] = {
	//Raw 10bit 3264*2448 30fps 4lane 720M bps/lane  
//MIPI=720Mbps, SysClk=148.8Mhz,Dac Clock=360Mhz.
//  {0x0100, 0x00},
  {0x030e, 0x00},
  {0x0312, 0x01},
  {0x3015, 0x01},
  {0x3700, 0x30},
  {0x3701, 0x18},
  {0x3702, 0x50},
  {0x3703, 0x32},
  {0x3704, 0x28},
  {0x3707, 0x08},
  {0x3708, 0x48},
  {0x3709, 0x66},
  {0x370c, 0x07},
  {0x3718, 0x14},
  {0x3712, 0x44},
  {0x371e, 0x31},
  {0x371f, 0x7f},
  {0x3720, 0x0a},
  {0x3721, 0x0a},
  {0x3724, 0x0c},
  {0x3725, 0x02},
  {0x3726, 0x0c},
  {0x3728, 0x0a},
  {0x3729, 0x03},
  {0x372a, 0x06},
  {0x372b, 0xa6},
  {0x372c, 0xa6},
  {0x372d, 0xa6},
  {0x372e, 0x0c},
  {0x372f, 0x20},
  {0x3730, 0x02},
  {0x3731, 0x0c},
  {0x3732, 0x28},
  {0x3736, 0x30},
  {0x373a, 0x0a},
  {0x373b, 0x0b},
  {0x373c, 0x14},
  {0x373e, 0x06},
  {0x375a, 0x0c},
  {0x375b, 0x26},
  {0x375d, 0x04},
  {0x375f, 0x28},
  {0x3772, 0x46},
  {0x3773, 0x04},
  {0x3774, 0x2c},
  {0x3775, 0x13},
  {0x3776, 0x08},
  {0x3778, 0x17},
  {0x37a0, 0x88},
  {0x37a1, 0x7a},
  {0x37a2, 0x7a},
  {0x37a7, 0x88},
  {0x37a8, 0x98},
  {0x37a9, 0x98},
  {0x37aa, 0x88},
  {0x37ab, 0x5c},
  {0x37ac, 0x5c},
  {0x37ad, 0x55},
  {0x37ae, 0x19},
  {0x37af, 0x19},
  {0x37b3, 0x84},
  {0x37b4, 0x84},
  {0x37b5, 0x60},
  {0x3768, 0x22},
  {0x3769, 0x44},
  {0x376a, 0x44},
  {0x3808, 0x0c},
  {0x3809, 0xc0},
  {0x380a, 0x09},
  {0x380b, 0x90},
  {0x380c, 0x07},
  {0x380d, 0x94},
  {0x380e, 0x09},
  {0x380f, 0xfc},
  {0x3814, 0x01},
//  {0x3821, 0x46},
  {0x3820, 0x06},
  {0x3821, 0x40},//modify by zhaocuiqin for CR01432234 20141231
  {0x382a, 0x01},
  {0x382b, 0x01},
  {0x3830, 0x06},
  {0x3836, 0x01},
  {0x3f08, 0x10},
  {0x4001, 0x00},
  {0x4020, 0x00},
  {0x4021, 0x04},
  {0x4022, 0x0b},
  {0x4023, 0xc3},
  {0x4024, 0x0c},
  {0x4025, 0x36},
  {0x4026, 0x0c},
  {0x4027, 0x37},
  {0x402a, 0x04},
  {0x402b, 0x08},
  {0x402e, 0x0c},
  {0x402f, 0x02},
  {0x4600, 0x01},
  {0x4601, 0x97},
  {0x5901, 0x00},
  {0x382d, 0xff},
//  {0x0100, 0x01},
};

static struct msm_camera_i2c_reg_array res1_reg_array[] = {                        
//Raw 10bit 1632x1224 30fps 4lane 720M bps/lane                 
//MIPI=720Mbps, SysClk=74.4Mhz,Dac Clock=360Mhz.       
//  {0x0100, 0x00},                                           
  {0x030e, 0x02},                         
  {0x0312, 0x03},                         
  {0x3015, 0x00},                         
  {0x3700, 0x18},                         
  {0x3701, 0x0c},                                             
  {0x3702, 0x28},                                             
  {0x3703, 0x19},                                             
  {0x3704, 0x14},                                             
  {0x3707, 0x04},                                             
  {0x3708, 0x24},                                             
  {0x3709, 0x33},                                             
  {0x370c, 0x04},                                             
  {0x3718, 0x12},                                             
  {0x3712, 0x42},                                             
  {0x371e, 0x19},                                             
  {0x371f, 0x40},                                             
  {0x3720, 0x05},                                             
  {0x3721, 0x05},                                             
  {0x3724, 0x06},                                             
  {0x3725, 0x01},                                             
  {0x3726, 0x06},                                             
  {0x3728, 0x05},                                             
  {0x3729, 0x02},                                             
  {0x372a, 0x03},                                             
  {0x372b, 0x53},                                             
  {0x372c, 0xa3},                                             
  {0x372d, 0x53},                                             
  {0x372e, 0x06},                                             
  {0x372f, 0x10},                                             
  {0x3730, 0x01},                                             
  {0x3731, 0x06},                                             
  {0x3732, 0x14},                                             
  {0x3736, 0x20},                                             
  {0x373a, 0x05},                                             
  {0x373b, 0x06},                                             
  {0x373c, 0x0a},                                             
  {0x373e, 0x03},                                             
  {0x375a, 0x06},                                             
  {0x375b, 0x13},                                             
  {0x375d, 0x02},                                             
  {0x375f, 0x14},                                             
  {0x3772, 0x23},                                             
  {0x3773, 0x02},                                             
  {0x3774, 0x16},                                             
  {0x3775, 0x12},                                             
  {0x3776, 0x04},                                             
  {0x3778, 0x17},                                             
  {0x37a0, 0x44},                                             
  {0x37a1, 0x3d},                                             
  {0x37a2, 0x3d},                                             
  {0x37a7, 0x44},                                             
  {0x37a8, 0x4c},                                             
  {0x37a9, 0x4c},                                             
  {0x37aa, 0x44},                                             
  {0x37ab, 0x2e},                                             
  {0x37ac, 0x2e},                                             
  {0x37ad, 0x33},                                             
  {0x37ae, 0x0d},                                             
  {0x37af, 0x0d},                                             
  {0x37b3, 0x42},                                             
  {0x37b4, 0x42},                                             
  {0x37b5, 0x31},                                             
  {0x3768, 0x22},                                             
  {0x3769, 0x44},                                             
  {0x376a, 0x44},                                             
  {0x3808, 0x06},                                             
  {0x3809, 0x60},            
  {0x380a, 0x04},            
  {0x380b, 0xc8},            
  {0x380c, 0x07},            
  {0x380d, 0x88},            
  {0x380e, 0x05},            
  {0x380f, 0x04},            
  {0x3814, 0x03},            
//  {0x3821, 0x67},     
  {0x3820, 0x06},   
  {0x3821, 0x61}, //modify by zhaocuiqin for CR01432234 20141231                                    
  {0x382a, 0x03},            
  {0x382b, 0x01},            
  {0x3830, 0x08},                 
  {0x3836, 0x02},                 
  {0x3843, 0x00},                 
  {0x3845, 0x02},                 
  {0x3f08, 0x08},                 
  {0x4001, 0x10},                 
  {0x4020, 0x00},                 
  {0x4021, 0x04},                 
  {0x4022, 0x04},                 
  {0x4023, 0xb9},                 
  {0x4024, 0x05},                 
  {0x4025, 0x2a},                 
  {0x4026, 0x05},                 
  {0x4027, 0x2b},                 
  {0x402a, 0x04},                                             
  {0x402b, 0x04},                                             
  {0x402e, 0x08},  
  {0x402f, 0x02},   
  {0x4600, 0x00},   
  {0x4601, 0xcb},   
  {0x5901, 0x00},   
  {0x382d, 0x7f},   
//  {0x0100, 0x01},                                            
};                                            


static struct msm_camera_i2c_reg_setting res_settings[] = {
  {
    .reg_setting = res0_reg_array,
    .size = ARRAY_SIZE(res0_reg_array),
    .addr_type = MSM_CAMERA_I2C_WORD_ADDR,
    .data_type = MSM_CAMERA_I2C_BYTE_DATA,
    .delay = 0,
  },
  {
    .reg_setting = res1_reg_array,
    .size = ARRAY_SIZE(res1_reg_array),
    .addr_type = MSM_CAMERA_I2C_WORD_ADDR,
    .data_type = MSM_CAMERA_I2C_BYTE_DATA,
    .delay = 0,
  },
};

static struct sensor_lib_reg_settings_array res_settings_array = {
  .reg_settings = res_settings,
  .size = ARRAY_SIZE(res_settings),
};

static struct sensor_crop_parms_t crop_params[] = {
  {0, 0, 0, 0}, /* RES 0 */
  {0, 0, 0, 0}, /* RES 1 */
};

static struct sensor_lib_crop_params_array crop_params_array = {
  .crop_params = crop_params,
  .size = ARRAY_SIZE(crop_params),
};

static struct sensor_lib_out_info_t sensor_out_info[] = {
  {
    .x_output = 3264,
    .y_output = 2448,
    .line_length_pclk = 1940,
    .frame_length_lines = 2556,
    .vt_pixel_clk = 148800000,
    .op_pixel_clk = 296000000,
    .binning_factor = 0,
    .max_fps = 30.0,
    .min_fps = 7.5,
    .mode = SENSOR_DEFAULT_MODE,

  },
  {
    .x_output = 1632,
    .y_output = 1224,
    .line_length_pclk = 1928,
    .frame_length_lines = 1284,
    .vt_pixel_clk = 74400000,
    .op_pixel_clk = 296000000,
    .binning_factor = 1,
    .max_fps = 30.0,
    .min_fps = 15.0,
    .mode = SENSOR_DEFAULT_MODE,
  },
};

static struct sensor_lib_out_info_array out_info_array = {
  .out_info = sensor_out_info,
  .size = ARRAY_SIZE(sensor_out_info),
};

static sensor_res_cfg_type_t ov8858_res_cfg[] = {
  SENSOR_SET_STOP_STREAM,
  SENSOR_SET_NEW_RESOLUTION, /* set stream config */
  SENSOR_SET_CSIPHY_CFG,
  SENSOR_SET_CSID_CFG,
  SENSOR_LOAD_CHROMATIX, /* set chromatix prt */
  SENSOR_SEND_EVENT, /* send event */
  SENSOR_SET_START_STREAM,
};

static struct sensor_res_cfg_table_t ov8858_res_table = {
  .res_cfg_type = ov8858_res_cfg,
  .size = ARRAY_SIZE(ov8858_res_cfg),
};

static struct sensor_lib_chromatix_t ov8858_chromatix[] = {
  {
    .common_chromatix = OV8858_LOAD_CHROMATIX(common),
    .camera_preview_chromatix = OV8858_LOAD_CHROMATIX(zsl),
    .camera_snapshot_chromatix = OV8858_LOAD_CHROMATIX(zsl),
    .camcorder_chromatix = OV8858_LOAD_CHROMATIX(default_video),
    .liveshot_chromatix = OV8858_LOAD_CHROMATIX(liveshot),
  },
  {
    .common_chromatix = OV8858_LOAD_CHROMATIX(common),
    .camera_preview_chromatix = OV8858_LOAD_CHROMATIX(preview),
    .camera_snapshot_chromatix = OV8858_LOAD_CHROMATIX(zsl),
    .camcorder_chromatix = OV8858_LOAD_CHROMATIX(default_video),
    .liveshot_chromatix = OV8858_LOAD_CHROMATIX(liveshot),
  },
};

static struct sensor_lib_chromatix_array ov8858_lib_chromatix_array = {
  .sensor_lib_chromatix = ov8858_chromatix,
  .size = ARRAY_SIZE(ov8858_chromatix),
};

#ifdef OV8858_VIDEO_UPSCALE_1080P
static msm_sensor_dimension_t ov8858_scale_size_tbl[] = {
  {2000, 1280},
};

/*===========================================================================
 * FUNCTION    - ov8858_get_scale_tbl -
 *
 * DESCRIPTION: Get scale size table
 *==========================================================================*/
static int32_t ov8858_get_scale_tbl(msm_sensor_dimension_t * tbl)
{
  int i;
  if(sensor_lib_ptr.scale_tbl_cnt == 0)
    return -1;
  for(i = 0; i < sensor_lib_ptr.scale_tbl_cnt; i++){
    tbl[i] = ov8858_scale_size_tbl[i];
  }

  return 0;
}
#endif
/*===========================================================================
/*===========================================================================
 * FUNCTION    - ov8858_real_to_register_gain -
 *
 * DESCRIPTION:
 *==========================================================================*/
static uint16_t ov8858_real_to_register_gain(float gain)
{
  uint16_t reg_gain;

  if (gain < 1.0) {
      gain = 1.0;
  } else if (gain > 8/*15.5*/) {
      gain = 8/*15.5*/;
  }
  gain = (gain) * 128.0;
  reg_gain = (uint16_t) gain;

  return reg_gain;
}


/*===========================================================================
 * FUNCTION    - ov8858_register_to_real_gain -
 *
 * DESCRIPTION:
 *==========================================================================*/
static float ov8858_register_to_real_gain(uint16_t reg_gain)
{
  float real_gain;

 if (reg_gain > 0x7ff) {
      reg_gain = 0x7ff;
  }
  real_gain = (float) reg_gain / 128.0;

  return real_gain;
}


/*===========================================================================
 * FUNCTION    - ov8858_calculate_exposure -
 *
 * DESCRIPTION:
 *==========================================================================*/
static int32_t ov8858_calculate_exposure(float real_gain,
  uint16_t line_count, sensor_exposure_info_t *exp_info)
{
  if (!exp_info) {
    return -1;
  }
  exp_info->reg_gain = ov8858_real_to_register_gain(real_gain);
  exp_info->sensor_real_gain =
    ov8858_register_to_real_gain(exp_info->reg_gain);
  exp_info->digital_gain = real_gain / exp_info->sensor_real_gain;
  exp_info->line_count = line_count;
  //exp_info->sensor_digital_gain = 0x1;
  return 0;
}

/*===========================================================================
 * FUNCTION    - ov8858_fill_exposure_array -
 *
 * DESCRIPTION:
 *==========================================================================*/
static int32_t ov8858_fill_exposure_array(uint16_t gain, uint32_t line,
  uint32_t fl_lines, int32_t luma_avg, uint32_t fgain,
  struct msm_camera_i2c_reg_setting* reg_setting)
{
  int32_t rc = 0;
  uint16_t reg_count = 0;
  uint16_t i = 0;

  if (!reg_setting) {
    return -1;
  }

  for (i = 0; i < sensor_lib_ptr.groupon_settings->size; i++) {
    reg_setting->reg_setting[reg_count].reg_addr =
      sensor_lib_ptr.groupon_settings->reg_setting[i].reg_addr;
    reg_setting->reg_setting[reg_count].reg_data =
      sensor_lib_ptr.groupon_settings->reg_setting[i].reg_data;
    reg_count = reg_count + 1;
  }

  reg_setting->reg_setting[reg_count].reg_addr =
    sensor_lib_ptr.output_reg_addr->frame_length_lines;
  reg_setting->reg_setting[reg_count].reg_data = (fl_lines & 0xFF00) >> 8;
  reg_count++;

  reg_setting->reg_setting[reg_count].reg_addr =
    sensor_lib_ptr.output_reg_addr->frame_length_lines + 1;
  reg_setting->reg_setting[reg_count].reg_data = (fl_lines & 0xFF);
  reg_count++;

  reg_setting->reg_setting[reg_count].reg_addr =
    sensor_lib_ptr.exp_gain_info->coarse_int_time_addr;
  reg_setting->reg_setting[reg_count].reg_data = (line & 0xffff) >> 12;
  reg_count++;

  reg_setting->reg_setting[reg_count].reg_addr =
    sensor_lib_ptr.exp_gain_info->coarse_int_time_addr + 1;
  reg_setting->reg_setting[reg_count].reg_data = (line & 0x0fff) >> 4;
  reg_count++;

  reg_setting->reg_setting[reg_count].reg_addr =
    sensor_lib_ptr.exp_gain_info->coarse_int_time_addr + 2;
  reg_setting->reg_setting[reg_count].reg_data = (line & 0x0f) << 4;
  reg_count++;

  reg_setting->reg_setting[reg_count].reg_addr =
    sensor_lib_ptr.exp_gain_info->global_gain_addr;
  reg_setting->reg_setting[reg_count].reg_data = (gain & 0x7FF) >> 8;
  reg_count++;

  reg_setting->reg_setting[reg_count].reg_addr =
    sensor_lib_ptr.exp_gain_info->global_gain_addr + 1;
  reg_setting->reg_setting[reg_count].reg_data = (gain & 0xFF);
  reg_count++;

  for (i = 0; i < sensor_lib_ptr.groupoff_settings->size; i++) {
    reg_setting->reg_setting[reg_count].reg_addr =
      sensor_lib_ptr.groupoff_settings->reg_setting[i].reg_addr;
    reg_setting->reg_setting[reg_count].reg_data =
      sensor_lib_ptr.groupoff_settings->reg_setting[i].reg_data;
    reg_count = reg_count + 1;
  }

  reg_setting->size = reg_count;
  reg_setting->addr_type = MSM_CAMERA_I2C_WORD_ADDR;
  reg_setting->data_type = MSM_CAMERA_I2C_BYTE_DATA;
  reg_setting->delay = 0;

  return rc;
}

static sensor_exposure_table_t ov8858_expsoure_tbl = {
  .sensor_calculate_exposure = ov8858_calculate_exposure,
  .sensor_fill_exposure_array = ov8858_fill_exposure_array,
};

static sensor_lib_t sensor_lib_ptr = {
  /* sensor slave info */
  .sensor_slave_info = &sensor_slave_info,
  /* sensor init params */
  .sensor_init_params = &sensor_init_params,
  /* sensor actuator name */
  //.actuator_name = "ROHM_BU64243GWZ",
  /* sensor output settings */
  .sensor_output = &sensor_output,
  /* sensor output register address */
  .output_reg_addr = &output_reg_addr,
  /* sensor exposure gain register address */
  .exp_gain_info = &exp_gain_info,
  /* sensor aec info */
  .aec_info = &aec_info,
  /* sensor snapshot exposure wait frames info */
  .snapshot_exp_wait_frames = 1,
  /* number of frames to skip after start stream */
  .sensor_num_frame_skip = 1,
  /* number of frames to skip after start HDR stream */
  .sensor_num_HDR_frame_skip = 2,
  /* sensor exposure table size */
  .exposure_table_size = 10,
  /* sensor lens info */
  .default_lens_info = &default_lens_info,
  /* csi lane params */
  .csi_lane_params = &csi_lane_params,
  /* csi cid params */
  .csi_cid_params = ov8858_cid_cfg,
  /* csi csid params array size */
  .csi_cid_params_size = ARRAY_SIZE(ov8858_cid_cfg),
  /* init settings */
  .init_settings_array = &init_settings_array,
  /* start settings */
  .start_settings = &start_settings,
  /* stop settings */
  .stop_settings = &stop_settings,
  /* group on settings */
  .groupon_settings = &groupon_settings,
  /* group off settings */
  .groupoff_settings = &groupoff_settings,
  /* resolution cfg table */
  .sensor_res_cfg_table = &ov8858_res_table,
  /* res settings */
  .res_settings_array = &res_settings_array,
  /* out info array */
  .out_info_array = &out_info_array,
  /* crop params array */
  .crop_params_array = &crop_params_array,
  /* csi params array */
  .csi_params_array = &csi_params_array,
  /* sensor port info array */
  .sensor_stream_info_array = &ov8858_stream_info_array,
  /* exposure funtion table */
  .exposure_func_table = &ov8858_expsoure_tbl,
  /* chromatix array */
  .chromatix_array = &ov8858_lib_chromatix_array,
  /* sensor pipeline immediate delay */
  .sensor_max_immediate_frame_delay = 2,
  #ifdef OV8858_VIDEO_UPSCALE_1080P
  /* scale size table count*/  
  .scale_tbl_cnt = ARRAY_SIZE(ov8858_scale_size_tbl),  
  /*function to get scale size tbl */  
  .get_scale_tbl = ov8858_get_scale_tbl,
  #endif
};

/*===========================================================================
 * FUNCTION    - ov8858_open_lib -
 *
 * DESCRIPTION:
 *==========================================================================*/
void *ov8858_open_lib(void)
{
  return &sensor_lib_ptr;
}
