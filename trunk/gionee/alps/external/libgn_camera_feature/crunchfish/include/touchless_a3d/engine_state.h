#ifndef ENGINE_STATE_H
#define ENGINE_STATE_H

namespace TouchlessA3D {
  namespace EngineState {

    /**
     * State of engine to indicate if engine is busy due to tracking objects or
     * has detected movement in the input images
     */
    enum type {
      /// Engine will run in low-power mode as no objects or movements has been detected.
      PASSIVE = 0,
      /// Engine has detected movement or is tracking object.
      ACTIVE = 1
    };

  } // EngineState
} // TouchlessA3D

#endif
