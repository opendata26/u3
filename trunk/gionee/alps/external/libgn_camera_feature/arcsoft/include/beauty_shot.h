/*----------------------------------------------------------------------------------------------
 *
 * This file is ArcSoft's property. It contains ArcSoft's trade secret, proprietary and
 * confidential information.
 *
 * The information and code contained in this file is only for authorized ArcSoft employees
 * to design, create, modify, or review.
 *
 * DO NOT DISTRIBUTE, DO NOT DUPLICATE OR TRANSMIT IN ANY FORM WITHOUT PROPER AUTHORIZATION.
 *
 * If you are not an intended recipient of this file, you must not copy, distribute, modify,
 * or take any action in reliance on it.
 *
 * If you have received this file in error, please immediately notify ArcSoft and
 * permanently delete the original and any copy of any file and any printout thereof.
 *
 *-------------------------------------------------------------------------------------------------*/
/*
 * beauty_shot.h
 *
 * Reference:
 *
 * Description:
 *
 *
 */

#ifndef _BEAUTY_SHOT_H_
#define _BEAUTY_SHOT_H_

#include "amcomdef.h"
#include "merror.h"
#include "asvloffscreen.h"
#include "abstypes.h"


class CBeautyShot
{
public:
	CBeautyShot();
	virtual ~CBeautyShot();

	MRESULT Init(DataType eType,MBool isNoFaceMode);
	MVoid Uninit();

	MRESULT LoadStyle(MVoid *pStyleData, MLong lDataSize);
	MRESULT GetStyleParam(BEAUTY_PARAM *pParam);

	MRESULT Detect(LPASVLOFFSCREEN pImgSrc, PAGE_GENDER_INFO** ppAgeGenderInfoArray, MLong *plArrayItemCnt);
	MRESULT Process(LPASVLOFFSCREEN pImgSrc, LPASVLOFFSCREEN pImgDst, MLong index, BEAUTY_PARAM *pParam);

private:
	MVoid		*m_pEngine;
	MHandle		m_hHandle;
};

#endif /* _BEAUTY_SHOT_H_ */
