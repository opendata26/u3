ifeq ("$(APK_TENCENTWECHAT_SUPPORT)","yes")
LOCAL_PATH := $(call my-dir)

###############################################################################
# GMS Mandatory Apps (not published in Play Store)
# This binary is required for any Google application to work.
# It MUST be installed on all devices.
###############################################################################
include $(CLEAR_VARS)
LOCAL_MODULE := com.tencent.mm_v6.1.0.66_r1062275_r542_00121_GIONEE_d3d6bf7aca7e4a495a03154ad722dcfa
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := $(LOCAL_MODULE).apk
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRIVILEGED_MODULE := true
LOCAL_CERTIFICATE := platform
include $(BUILD_PREBUILT)
endif
