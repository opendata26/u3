#!/bin/bash

cat /dev/null > /data/aurora/currentfile
cat /dev/null > /data/aurora/system_current_md5.file
sync
isDir(){
    local dirName=$1
    if [ ! -d $dirName ]; then
    return 1
    else
    return 0
    fi
}

recursionDir(){
    local dir=$1
    local filelist=`ls  "${dir}"`
    for filename in $filelist
    do
        local fullpath="$dir"/"${filename}";
        if isDir "${fullpath}";then
            recursionDir "${fullpath}"
        else
            echo "${fullpath}"  >> /data/aurora/currentfile
        fi
    done
}

echo "recurse current file."
recursionDir /system/app
recursionDir /system/priv-app
recursionDir /system/fonts
recursionDir /system/lib
recursionDir /system/bin
recursionDir /system/xbin
recursionDir /system/framework

echo "create current md5."
for file in `cat /data/aurora/currentfile`
do
   [ -z "${file}" -o "${file:0:1}" == "#" ] && continue
   tmp=`md5 ${file}`
   md5value=${tmp:0:32}
   echo "${file}:${md5value}" >> /data/aurora/system_current_md5.file
done
